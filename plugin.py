from Plugins.Plugin import PluginDescriptor

from enigma import eDVBDB, eServiceCenter
from Tools.Directories import resolveFilename, SCOPE_CONFIG
from Tools.ServiceReference import service_types_tv_ref, serviceRefAppendPath
from Components.ServiceList import refreshServiceList
from Screens.MessageBox import MessageBox

class NumberLCN():
	def __init__(self, session):
		self.session = session
		session.openWithCallback(self.confirm, MessageBox, _("Renumber Favourites to LCN?"), MessageBox.TYPE_YESNO, default=True)

	def confirm(self, confirmed):
		if not confirmed:
			return

		self.msg = _("Favourites renumbered!")  # assume success
		self.mbt = MessageBox.TYPE_ERROR		# or not
		self.serviceHandler = eServiceCenter.getInstance()
		self.lcndb = {}
		self.loadLCNdb()
		if self.lcndb:
			bouquet = serviceRefAppendPath(service_types_tv_ref, ' FROM BOUQUET "userbouquet.favourites.tv" ORDER BY bouquet')
			self.renumberBouquet(bouquet)
			eDVBDB.getInstance().renumberBouquet()
			refreshServiceList()
		self.session.open(MessageBox, self.msg, self.mbt, 5)

	def renumberBouquet(self, bouquet):
		servicelist = self.serviceHandler.list(bouquet)
		if servicelist is None:
			self.msg = _("Renumber failed: no Favourites.")
			return

		try:
			filename = bouquet.getPath().split('"', 3)[1] + ".numbers"
			f = open(resolveFilename(SCOPE_CONFIG, filename), "w")
		except Exception, e:
			print "[Numberer]", e
			self.msg = _("Renumber failed: unable to create numbers file.")
			return
		nolcn = 0
		while True:
			serviceIterator = servicelist.getNext()
			if not serviceIterator.valid():
				break
			namespace = serviceIterator.getUnsignedData(4)
			nid = serviceIterator.getUnsignedData(3)
			tsid = serviceIterator.getUnsignedData(2)
			sid = serviceIterator.getUnsignedData(1)
			lcn = self.lcndb.get((namespace, nid, tsid, sid))
			if not lcn:
				nolcn += 1
				lcn = 0
			print >> f, "#NUMBER %3d" % lcn, serviceIterator.toCompareString()
		f.close()
		if nolcn == 1:
			self.msg += _("\n\nFound one channel without an LCN.")
		elif nolcn:
			self.msg += _("\n\nFound %d channels without an LCN.") % nolcn
		self.mbt = MessageBox.TYPE_INFO

	# adapted from IniLCNScanner
	def loadLCNdb(self):
		try:
			f = open(resolveFilename(SCOPE_CONFIG, "lcndb"))
		except Exception, e:
			print "[Numberer]", e
			self.msg = _("Renumber failed: no LCN database.")
			return

		for line in f:
			line = line.strip()
			if len(line) != 38:
				continue

			tmp = line.split(":")
			if len(tmp) != 6:
				continue

			namespace = int(tmp[0], 16)
			nid = int(tmp[1], 16)
			tsid = int(tmp[2], 16)
			sid = int(tmp[3], 16)
			lcn = int(tmp[4])
			self.lcndb[(namespace, nid, tsid, sid)] = lcn

		f.close()
		if not self.lcndb:
			self.msg = _("Renumber failed: LCN database empty.")

def main(session, **kwargs):
	NumberLCN(session)

def Plugins(**kwargs):
	return PluginDescriptor(
		name="Numberer",
		description=_("Renumber Favourites to use LCNs."),
		where=PluginDescriptor.WHERE_PLUGINMENU,
		fnc=main)
